class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def forgot_password(user)
    @user = user
    @url = user_password_reset_url(@user) + "?session_token=" + @user.session_token

    mail(to: user.email, subject: 'Password Reset')
  end
end
