class UsersController < ApplicationController
  def index
    render :index
  end

  def new
    render :new
  end

  def create
    @user = User.new(params[:user])

    if @user.save
      log_in!(@user)
      redirect_to user_url(@user)
    else
      flash[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
    render :show
  end

  def password_reset
    @user = User.find_by_session_token(params[:session_token])
    log_in!(@user)
    render :password_reset
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes!(params[:user])
    redirect_to user_url(@user)
  end
end
