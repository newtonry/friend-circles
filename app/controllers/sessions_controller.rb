class SessionsController < ApplicationController

  def new
    render :new
  end

  def create
    @user = User.find_by_email(params[:user][:email])

    if @user.nil?
      render :new
    elsif @user.is_password?(params[:user][:password])
      log_in!(@user)
      redirect_to user_url(@user)
    else
      flash[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def destroy
    current_user.reset_session_token!
    session[:token] = nil
    redirect_to new_session_url
  end

  def send_reset_email
    @user = User.find_by_email(params[:user][:email])
    msg = UserMailer.forgot_password(@user)
    msg.deliver!
    render :new
  end
end
