class FriendCircleMembership < ActiveRecord::Base
  attr_accessible :friend_circle_id, :friend_id

  validates :friend_id, :friend_circle_id, presence: true

  belongs_to(:friend_circle,
  class_name: "FriendCircle",
  primary_key: :id,
  foreign_key: :friend_circle_id
  )

  has_many(:members,
  class_name: "User",
  primary_key: :id,
  foreign_key: :friend_id
  )

end
