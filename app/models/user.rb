class User < ActiveRecord::Base
  attr_accessible :email, :password_digest, :session_token, :password

  before_validation :session_token, :must_be_set
  validates :email, uniqueness: true
  validates :email, :password_digest, :session_token, presence: true

  has_many(:friend_circles,
  class_name: "FriendCircle",
  primary_key: :id,
  foreign_key: :owner_id
  )

  has_many(:memberships,
  class_name: "FriendCircleMembership",
  primary_key: :id,
  foreign_key: :friend_id
  )

  def reset_session_token!
    self.session_token = SecureRandom::urlsafe_base64(16)
    self.save!
  end

  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
     BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  private

  def must_be_set
    reset_session_token! if self.session_token.nil?
  end
end
