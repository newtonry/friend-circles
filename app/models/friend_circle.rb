class FriendCircle < ActiveRecord::Base
  attr_accessible :owner_id, :name

  validates :owner_id, :name, presence: true

  belongs_to(:owner,
  class_name: "User",
  primary_key: :id,
  foreign_key: :owner_id
  )

  has_many(:friend_circle_memberships,
  class_name: "FriendCircleMembership",
  primary_key: :id,
  foreign_key: :friend_circle_id
  )

end
