module SessionsHelper
  def current_user
    User.find_by_session_token(session[:token])
  end

  def log_in!(user)
    user.reset_session_token!
    session[:token] = user.session_token
  end

  def logged_in?
    !current_user.nil?
  end
end
