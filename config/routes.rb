SocialApp::Application.routes.draw do
  resources :users do
    resources :friend_circles
    get "password_reset"
  end

  resource :session do
    post "send_reset_email"
  end
end
